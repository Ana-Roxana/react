import React from 'react';
import logo from './logo.svg';
import './App.css';
import Buttons from './containers/Buttons'
import ButtonSAP from './containers/ButtonSAP'
import "@ui5/webcomponents/dist/ShellBar";
import "@ui5/webcomponents/dist/Input.js";
import Inputs from './containers/input'

let x=3;

function App() {
  return (
 <div >   
           
 <ui5-shellbar	primary-title="My aplication"	secondary-title="by Roxi"></ui5-shellbar>

<div className="row">
<div style ={{width:"20%"}} className="column">
<ui5-list id="myList" class="full-width">
	<ui5-li icon="sap-icon://nutrition-activity" description="Tropical plant with an edible fruit" info="In-stock" info-state="Success">Pineapple</ui5-li>
	<ui5-li icon="sap-icon://nutrition-activity" description="Occurs between red and yellow" info="Expires" info-state="Warning">Orange</ui5-li>
	<ui5-li icon="sap-icon://nutrition-activity" description="The yellow lengthy fruit" info="Re-stock" info-state="Error">Banana</ui5-li>
	<ui5-li icon="sap-icon://nutrition-activity" description="The tropical stone fruit" info="Re-stock" info-state="Error">Mango</ui5-li>
</ui5-list>
</div>
<div className="column" style ={{width:"30%"}}>
  <Inputs></Inputs>
  <Inputs></Inputs>

</div>

</div>

    </div>
  );
}

export default App;
